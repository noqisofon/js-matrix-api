const express    = require( 'express' );
const bodyParser = require( 'body-parser' );

const app        = express();

// body-parser の設定をするよ！するよ！
app.use( bodyParser.urlencoded( { extended: true } ) );
app.use( bodyParser.json() );

// ここら辺でポート番号を宣言しておく。
const port       = process.env.PORT || 3000;

// GET /${domain}:${port}/_matrix/client/versions
app.get( '/_matrix/client/versions', (request, response) => {
    response.json( {
        versions: 'r0.0.1'
    } );
} );

// GET /${domain}:${port}/.well-known/matrix/client
app.get( '/.well-known/matrix/client', (request, response) => {
    response.json( {
        'm.homeserver': {
            base_url: 'https://matrix.example.com'
        },
        'm.identity_server': {
            base_url: 'https://identity.example.com'
        }
    } );
} );


// ルートを定義したので、鯖を起動するよ！！
app.listen( port );

console.log( 'listen on port: %d', port );
